/**
 * @file
 * Replace title with AP-style and copy to 'index' title.
 */
(function (Drupal, drupalSettings) {
  "use strict";

  Drupal.titlecasesuggestions = {};

    /**
     * Copies title to secondary field, if provided.
     */
    Drupal.titlecasesuggestions.copy = function(title) {
        // @TODO get ID from settings page where site builders select field.
        var copytoId = 'edit-field-index-title-0-value';
        var copyto = document.getElementById(copytoId);
        if (!copyto.value) {
            copyto.value = title.value;
        }
    }

    Drupal.titlecasesuggestions.fetchTitle = function() {
        var checkboxId = 'edit-titlecasesuggestions-checkbox';
        var checkbox = document.getElementById(checkboxId);
        // @TODO? Allow 'title' field to be selected by site builder.
        var titleId = 'edit-title-0-value';
        var title = document.getElementById(titleId);
        if (title.value) {
            // If we don't have an API key we can only do the copy.  Likewise,
            // if "Automatic conversion of title to AP headline casing is
            // active" is not checked, we only copy the title.
            if (!drupalSettings.titlecasesuggestions.rapidapi_key || !checkbox.checked) {
                Drupal.titlecasesuggestions.copy(title);
                return;
            }
            // We have the API key so we will get our Title-Cased title.
            var titleEncoded = encodeURIComponent(title.value);
            fetch("https://title-case-converter.p.rapidapi.com/v1/TitleCase?title=" + titleEncoded + "&style=AP&preserveAllCaps=auto", {
                "method": "GET",
                "headers": {
                    "x-rapidapi-host": "title-case-converter.p.rapidapi.com",
                    "x-rapidapi-key": drupalSettings.titlecasesuggestions.rapidapi_key
                }
            }).then(response => response.json().then(body => {
                let casedTitle = body.result;
                if (casedTitle) {
                    title.value = casedTitle;
                }
                Drupal.titlecasesuggestions.copy(title);
            })).catch(err => {
                console.error(err);
                Drupal.titlecasesuggestions.copy(title);
            });
        };
    }

    Drupal.behaviors.titleCaseSuggestions = {
        attach: function (context, settings) {
            // Elements passed in by 'once' are text not DOM elements so we ignore.

            var checkboxId = 'edit-titlecasesuggestions-checkbox';
            var checkbox = document.getElementById(checkboxId);
            // @TODO? Allow 'title' field to be selected by site builder.
            var titleId = 'edit-title-0-value';
            var title = document.getElementById(titleId);
            title.addEventListener("blur", Drupal.titlecasesuggestions.fetchTitle);
            checkbox.addEventListener("click", Drupal.titlecasesuggestions.fetchTitle);
        }
    }
} (Drupal, drupalSettings));
